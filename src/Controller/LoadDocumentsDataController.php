<?php

namespace Drupal\privacy_docs_world_map\Controller;

use Drupal\views\Views;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\Request;

/**
 * An LoadDocumentsDataController controller.
 */
class LoadDocumentsDataController extends ControllerBase {

  /**
   * Return the privacy document for clicked country region.
   */
  public function getCountryDocuments(Request $request) {
    if ((NULL !== $request->query->get('country_code'))) {
      $data = [];
      $country_code = $request->query->get('country_code');
      $view = Views::getView('privacy_docs');
      $view->setExposedInput([
        'field_select_country_state_country_code' => $country_code,
      ]);
      $view->setDisplay('block_1');
      $view->execute();
      // Get the results of the view.
      $view_result = $view->result;
      foreach ($view_result as $result) {
        foreach ($result->_entity->field_privacy_document as $key => $doc) {
          $file_path = $doc->entity->createFileUrl(FALSE);
          $data['files'][] = [
            'file_url' => $file_path,
            'file_size' => $doc->entity->getSize(),
            'file_type' => pathinfo($file_path, PATHINFO_EXTENSION),
            'file_description' => $doc->description != "" ? $doc->description : $doc->entity->getFilename(),
          ];
        }
      }
      $cacheable_metadata = CacheableMetadata::createFromRenderArray([
        '#cache' => [
          'tags' => [
            'country_docs:' . $country_code,
          ],
          'contexts' => [
            'url.path',
            'url.query_args:country_code',
            'url.query_args:country_name',
          ],
        ],
      ]);
      $response = new CacheableJsonResponse($data);
      $response->addCacheableDependency($cacheable_metadata);
      return $response;
    }

  }

}
