<?php

namespace Drupal\privacy_docs_world_map\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Provides a 'privacy_docs_world_map' block.
 *
 * @Block(
 *   id = "privacy_docs_world_map_block",
 *   admin_label = @Translation("Privacy Docs World Map Block"),
 *
 * )
 */
class PrivacyDoucmentsWorldMapBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * ConfigFactory variable.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configfactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
        $configuration,
        $plugin_id,
        $plugin_definition,
        $container->get('config.factory'));
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configfactory = $config_factory->get('privacy_docs_world_map.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Do something.
    return [
      '#theme' => 'privacy_docs_world_map_block',
      '#world_map_default_text' => $this->configfactory->get('world_map_default_text'),
    ];
  }

}
