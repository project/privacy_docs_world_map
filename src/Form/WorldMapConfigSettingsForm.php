<?php

namespace Drupal\privacy_docs_world_map\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class used to store the cookies config settings.
 */
class WorldMapConfigSettingsForm extends ConfigFormBase {

  /**
   * Config variable {@var string Config settings}.
   */
  const SETTINGS = 'privacy_docs_world_map.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'privacy_docs_world_map_settings_id';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config(static::SETTINGS);
    $form['world_map_default_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default text'),
      '#default_value' => $config->get('world_map_default_text'),
      '#size' => 250,
      '#required' => TRUE,
    ];

    $form['world_map_no_result_found'] = [
      '#type' => 'textfield',
      '#title' => $this->t('No result text'),
      '#default_value' => $config->get('world_map_no_result_found'),
      '#size' => 250,
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save Settings'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $config = $this->configFactory->getEditable(static::SETTINGS);

    // Set the submitted configuration setting.
    $config
      ->set('world_map_no_result_found', $form_state->getValue('world_map_no_result_found'))
      ->set('world_map_default_text', $form_state->getValue('world_map_default_text'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
